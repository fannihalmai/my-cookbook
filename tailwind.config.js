/* eslint-disable no-undef */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        "cook-primary": "#f9e0ff",
        "cook-secondary": "#dbf1ff",
        "cook-yellow": "#ffdc5e",
        "cook-pink": "#ff6379",
        "cook-green": "	#96ffaa",
        "cook-beige": "	#fbf2f5",
        "cook-peach": "	#ffe4e4"
      },
    },
    fontFamily: {
      Roboto: ["Roboto, sans-serif"],
    },
    container: {
      padding: "2rem",
      center: true,
    },
    screens: {
      sm: "640px",
      md: "768px",
    },
    plugins: [],
  },
};
