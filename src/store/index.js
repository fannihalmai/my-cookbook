import { createStore } from 'vuex'

export const store = createStore({
    state () {
      return {
        savedRecipes : []
      }
    },
    getters: {
        getRecipes (state) {
          return state.savedRecipes;
        },
        getRecipe(state, payload){
            
        }
    },
    mutations: {
      saveRecipe(state){

      },
      deleteFromFavorites(state, payload){
        console.log(state.savedRecipes , payload);
        state.savedRecipes = state.savedRecipes.filter(el => el.id != payload)
        console.log(state.savedRecipes);

      },
      setSavedRecipes(state, payload){
        state.savedRecipes = payload
      },
      addToFavorites(state, payload){
        state.savedRecipes.push(payload)
      }
    }
  })